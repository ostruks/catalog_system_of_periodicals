﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DAL.Reposytory
{
    public interface IGenericRepository<T> where T : class
    {
        void Create(T item);
        T FindById(int id);
        T FindByIdWithIncludes(int id, string[] includes);
        IEnumerable<T> Get();
        IEnumerable<T> Get(Func<T, bool> predicate);
        void Remove(T item);
        void Update(T item);
        IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
            params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetWithInclude(string table1, string table2 = "");
    }
}
