﻿using System.Data.Entity;

namespace DAL.Models
{
    public class PeriodicalContext : DbContext
    {
        public PeriodicalContext() :
            base("DefaultConnection")
        { }

        public DbSet<AuthorDTO> Authors { get; set; }
        public DbSet<GenreDTO> Genres { get; set; }
        public DbSet<PeriodicalDTO> Periodicals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorDTO>()
            .HasMany(p => p.Periodicals)
            .WithRequired(p => p.Author)
            .HasForeignKey(s => s.AuthorId)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<PeriodicalDTO>()
            .HasMany(p => p.Genres)
            .WithMany(c => c.Periodicals)
            .Map(m =>
            {
                m.ToTable("PeriodicalGenres");
                m.MapLeftKey("GenreId");
                m.MapRightKey("PeriodicalId");
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}