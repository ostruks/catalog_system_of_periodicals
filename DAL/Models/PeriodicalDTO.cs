﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    [Table("Periodical")]
    public class PeriodicalDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? PublicationFrequencyDays { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public AuthorDTO Author { get; set; }
        public ICollection<GenreDTO> Genres { get; set; }

        public PeriodicalDTO()
        {
            Genres = new List<GenreDTO>();
        }
    }
}