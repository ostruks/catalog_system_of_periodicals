﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    [Table("Author")]
    public class AuthorDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Avatar { get; set; }
        public string UserId { get; set; }
        public ICollection<PeriodicalDTO> Periodicals { get; set; }

        public AuthorDTO()
        {
            Periodicals = new List<PeriodicalDTO>();
        }
    }
}
