﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    [Table("Genre")]
    public class GenreDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<PeriodicalDTO> Periodicals { get; set; }

        public GenreDTO()
        {
            Periodicals = new List<PeriodicalDTO>();
        }
    }
}
