﻿namespace DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddTablesMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Author",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        NickName = c.String(),
                        Avatar = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Periodical",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        PublicationFrequencyDays = c.Int(),
                        DownloadLink = c.String(),
                        Description = c.String(),
                        AuthorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Author", t => t.AuthorId, cascadeDelete: true)
                .Index(t => t.AuthorId);
            
            CreateTable(
                "dbo.Genre",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PeriodicalGenres",
                c => new
                    {
                        GenreId = c.Int(nullable: false),
                        PeriodicalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GenreId, t.PeriodicalId })
                .ForeignKey("dbo.Periodical", t => t.GenreId, cascadeDelete: true)
                .ForeignKey("dbo.Genre", t => t.PeriodicalId, cascadeDelete: true)
                .Index(t => t.GenreId)
                .Index(t => t.PeriodicalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Periodical", "AuthorId", "dbo.Author");
            DropForeignKey("dbo.PeriodicalGenres", "PeriodicalId", "dbo.Genre");
            DropForeignKey("dbo.PeriodicalGenres", "GenreId", "dbo.Periodical");
            DropIndex("dbo.PeriodicalGenres", new[] { "PeriodicalId" });
            DropIndex("dbo.PeriodicalGenres", new[] { "GenreId" });
            DropIndex("dbo.Periodical", new[] { "AuthorId" });
            DropTable("dbo.PeriodicalGenres");
            DropTable("dbo.Genre");
            DropTable("dbo.Periodical");
            DropTable("dbo.Author");
        }
    }
}
