﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeColumnNamePeriodicalTableDownLoadLinkToText : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Periodical", "Text", c => c.String());
            DropColumn("dbo.Periodical", "DownloadLink");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Periodical", "DownloadLink", c => c.String());
            DropColumn("dbo.Periodical", "Text");
        }
    }
}
