﻿using DAL.Models;
using LightInject;
using System.Data.Entity;

namespace DAL.LightInject
{
    public static class DALLightInjectCongiguration
    {
        public static ServiceContainer Congiguration(ServiceContainer container)
        {
            container.Register(typeof(DbContext), typeof(PeriodicalContext));
            return container;
        }
    }
}
