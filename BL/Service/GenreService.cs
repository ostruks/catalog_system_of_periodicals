﻿using AutoMapper;
using BL.Models;
using DAL.Models;
using DAL.Reposytory;
using System.Collections.Generic;

namespace BL.Service
{
    public class GenreService : GenericService<GenreBL, GenreDTO>, IService<GenreBL>
    {
        private readonly IMapper _mapper;

        public GenreService(IGenericRepository<GenreDTO> repository, IMapper mapper) : base(repository)
        {
            _mapper = mapper;
        }

        public override GenreBL Map(GenreDTO model)
        {
            return _mapper.Map<GenreBL>(model);
        }

        public override GenreDTO Map(GenreBL model)
        {
            return _mapper.Map<GenreDTO>(model);
        }

        public override IEnumerable<GenreBL> Map(IEnumerable<GenreDTO> entitiesList)
        {
            return _mapper.Map<IEnumerable<GenreBL>>(entitiesList);
        }

        public override IEnumerable<GenreDTO> Map(IEnumerable<GenreBL> entitiesList)
        {
            return _mapper.Map<IEnumerable<GenreDTO>>(entitiesList);
        }
    }
}
