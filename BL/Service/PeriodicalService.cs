﻿using AutoMapper;
using BL.Models;
using DAL.Models;
using DAL.Reposytory;
using System.Collections.Generic;

namespace BL.Service
{
    public class PeriodicalService : GenericService<PeriodicalBL, PeriodicalDTO>, IService<PeriodicalBL>
    {
        private readonly IMapper _mapper;

        public PeriodicalService(IGenericRepository<PeriodicalDTO> repository, IMapper mapper) : base(repository)
        {
            _mapper = mapper;
        }

        public override PeriodicalBL Map(PeriodicalDTO model)
        {
            return _mapper.Map<PeriodicalBL>(model);
        }

        public override PeriodicalDTO Map(PeriodicalBL model)
        {
            return _mapper.Map<PeriodicalDTO>(model);
        }

        public override IEnumerable<PeriodicalBL> Map(IEnumerable<PeriodicalDTO> entitiesList)
        {
            return _mapper.Map<IEnumerable<PeriodicalBL>>(entitiesList);
        }

        public override IEnumerable<PeriodicalDTO> Map(IEnumerable<PeriodicalBL> entitiesList)
        {
            return _mapper.Map<IEnumerable<PeriodicalDTO>>(entitiesList);
        }
    }
}
