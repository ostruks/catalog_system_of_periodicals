﻿using AutoMapper;
using BL.Models;
using DAL.Models;
using DAL.Reposytory;
using System.Collections.Generic;

namespace BL.Service
{
    public class AuthorService : GenericService<AuthorBL, AuthorDTO>, IService<AuthorBL>
    {
        private readonly IMapper _mapper;

        public AuthorService(IGenericRepository<AuthorDTO> repository, IMapper mapper) : base(repository)
        {
            _mapper = mapper;
        }

        public override AuthorBL Map(AuthorDTO model)
        {
            return _mapper.Map<AuthorBL>(model);
        }

        public override AuthorDTO Map(AuthorBL model)
        {
            return _mapper.Map<AuthorDTO>(model);
        }

        public override IEnumerable<AuthorBL> Map(IEnumerable<AuthorDTO> entitiesList)
        {
            return _mapper.Map<IEnumerable<AuthorBL>>(entitiesList);
        }

        public override IEnumerable<AuthorDTO> Map(IEnumerable<AuthorBL> entitiesList)
        {
            return _mapper.Map<IEnumerable<AuthorDTO>>(entitiesList);
        }
    }
}
