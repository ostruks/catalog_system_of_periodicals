﻿using System;
using System.Collections.Generic;

namespace BL.Service
{
    public interface IGenereicService<BLModel> where BLModel : class
    {
        BLModel FindById(int id);
        IEnumerable<BLModel> GetAll();
        IEnumerable<BLModel> GetWithInclude(string table1, string table2 = "");
        BLModel FindByIdWithIncludes(int id, string[] includes);
        void Create(BLModel item);
        void DeleteById(int id);
        void Delete(BLModel item);
        void Update(BLModel item);
    }
}
