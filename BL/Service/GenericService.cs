﻿using DAL.Reposytory;
using System;
using System.Collections.Generic;

namespace BL.Service
{
    public abstract class GenericService<BLModel, DALModel> : IGenereicService<BLModel>
        where BLModel : class
        where DALModel : class
    {
        private readonly IGenericRepository<DALModel> _repositroy;
        public GenericService(IGenericRepository<DALModel> repository)
        {
            _repositroy = repository;
        }

        public void Create(BLModel item)
        {
            _repositroy.Create(Map(item));
        }

        public void Update(BLModel item)
        {
            _repositroy.Update(Map(item));
        }

        public void DeleteById(int id)
        {
            var entity = _repositroy.FindById(id);
            _repositroy.Remove(entity);
        }

        public void Delete(BLModel item)
        {
            _repositroy.Remove(Map(item));
        }

        public virtual BLModel FindById(int id)
        {
            var entity = _repositroy.FindById(id);
            return Map(entity);
        }

        public virtual BLModel FindByIdWithIncludes(int id, string[] includes)
        {
            var entity = _repositroy.FindByIdWithIncludes(id, includes);
            return Map(entity);
        }

        public IEnumerable<BLModel> GetAll()
        {
            var listEntity = _repositroy.Get();
            return Map(listEntity);
        }

        public IEnumerable<BLModel> GetWithInclude(string table1, string table2 = "")
        {
            var listEntity = _repositroy.GetWithInclude(table1, table2);
            return Map(listEntity);
        }

        /// <summary>
        /// convert Dal model to Bl molder
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public abstract BLModel Map(DALModel entity);
        public abstract DALModel Map(BLModel blmodel);
        public abstract IEnumerable<BLModel> Map(IEnumerable<DALModel> entity);
        public abstract IEnumerable<DALModel> Map(IEnumerable<BLModel> entity);
    }
}
