﻿using System.Collections.Generic;

namespace BL.Models
{
    public class PeriodicalBL
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? PublicationFrequencyDays { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public AuthorBL Author { get; set; }
        public ICollection<GenreBL> Genres { get; set; }
    }
}