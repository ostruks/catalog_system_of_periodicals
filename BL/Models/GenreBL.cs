﻿using System.Collections.Generic;

namespace BL.Models
{
    public class GenreBL
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<PeriodicalBL> Periodicals { get; set; }
    }
}
