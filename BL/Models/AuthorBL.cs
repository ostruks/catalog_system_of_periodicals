﻿using System.Collections.Generic;

namespace BL.Models
{
    public class AuthorBL
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Avatar { get; set; }
        public string UserId { get; set; }

        public ICollection<PeriodicalBL> Periodicals { get; set; }
    }
}
