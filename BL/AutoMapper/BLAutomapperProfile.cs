﻿using AutoMapper;
using BL.Models;
using DAL.Models;

namespace BL.AutoMapper
{
    public class BLAutomapperProfile : Profile
    {
        public BLAutomapperProfile()
        {
            CreateMap<AuthorBL, AuthorDTO>().ReverseMap();
            CreateMap<GenreBL, GenreDTO>().ReverseMap();
            CreateMap<PeriodicalBL, PeriodicalDTO>().ReverseMap();
        }
    }
}
