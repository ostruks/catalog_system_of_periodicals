﻿using DAL.LightInject;
using DAL.Models;
using DAL.Reposytory;
using LightInject;

namespace BL.LightInject
{
    public static class BLLightInjectCongiguration
    {
        public static ServiceContainer Congiguration(ServiceContainer container)
        {
            container = DALLightInjectCongiguration.Congiguration(container);

            container.Register(typeof(IGenericRepository<AuthorDTO>), typeof(GenericRepository<AuthorDTO>));
            container.Register(typeof(IGenericRepository<GenreDTO>), typeof(GenericRepository<GenreDTO>));
            container.Register(typeof(IGenericRepository<PeriodicalDTO>), typeof(GenericRepository<PeriodicalDTO>));
            return container;
        }
    }
}
