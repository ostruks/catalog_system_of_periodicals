﻿using AutoMapper;
using BL.Models;
using System.Collections.Generic;
using System.Linq;
using Web_mvc.Models;

namespace Web_mvc.LightInject
{
    public class WebAutomapperProfile : Profile
    {
        public WebAutomapperProfile()
        {
            CreateMap<AuthorBL, AuthorViewModel>()
                .ForMember(dest => dest.FullName,
                   opts => opts.MapFrom(
                       src => string.Format("{0} {1}",
                           src.FirstName,
                           src.LastName)));
            CreateMap<AuthorBL, AuthorCreateModel>().ReverseMap();
            CreateMap<GenreBL, GenreCreateModel>().ReverseMap();
            CreateMap<GenreBL, GenreViewModel>().ReverseMap();
            CreateMap<IEnumerable<GenreCreateModel>, IEnumerable<GenreViewModel>>().ReverseMap();
            CreateMap<PeriodicalBL, PeriodicalViewModel>()
                .ForMember(dest => dest.Author,
                   opts => opts.MapFrom(
                       src => new AuthorViewModel
                       {
                           Id = src.Author.Id,
                           FullName = $"{src.Author.FirstName} {src.Author.LastName}",
                           NickName = src.Author.NickName,
                           Avatar = src.Author.Avatar,
                           UserId = src.Author.UserId
                       }))
                .ForMember(dest => dest.Genres,
                   opts => opts.MapFrom(
                       src => src.Genres));
            CreateMap<PeriodicalBL, PeriodicalCreateModel>()
                .ForMember(dest => dest.Author,
                   opts => opts.MapFrom(
                       src => new AuthorCreateModel
                       {
                           Id = src.Author.Id,
                           FirstName = src.Author.FirstName,
                           LastName = src.Author.LastName,
                           NickName = src.Author.NickName,
                           Avatar = src.Author.Avatar,
                           UserId = src.Author.UserId
                       }))
                .ForMember(dest => dest.Genres,
                   opts => opts.MapFrom(
                       src => src.Genres)).ReverseMap();
        }
    }
}