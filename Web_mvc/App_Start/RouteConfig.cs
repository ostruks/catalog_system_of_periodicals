﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Web_mvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "PeriodicalDelete",
                url: "Periodical/Delete/{id}",
                defaults: new { controller = "Periodical", action = "Delete", id = UrlParameter.Optional },
                constraints: new { id = @"\d+" }
            );

            routes.MapRoute(
                name: "PeriodicalEdit",
                url: "Periodical/Edit/{id}",
                defaults: new { controller = "Periodical", action = "Edit", id = UrlParameter.Optional },
                constraints: new { id = @"\d+" }
            );

            routes.MapRoute(
                name: "Periodical",
                url: "Periodical/{id}",
                defaults: new { controller = "Periodical", action = "Details", id = UrlParameter.Optional },
                constraints: new { id = @"\d+" }
            );

            routes.MapRoute(
                name: "Author",
                url: "Author/{id}",
                defaults: new { controller = "Author", action = "Details", id = UrlParameter.Optional },
                constraints: new { id = @"\d+" }
            );

            routes.MapRoute(
                name: "Genre",
                url: "Genre/{id}",
                defaults: new { controller = "Genre", action = "Details", id = UrlParameter.Optional },
                constraints: new { id = @"\d+" }
            );

            routes.MapRoute(
                name: "PeriodicalIndexAll",
                url: "Periodical",
                defaults: new { controller = "Periodical", action = "Index" }
            );

            routes.MapRoute(
                name: "PeriodicalGetAll",
                url: "Periodical/GetAll",
                defaults: new { controller = "Periodical", action = "GetAll" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
