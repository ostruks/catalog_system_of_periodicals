﻿using AutoMapper;
using BL.AutoMapper;
using BL.LightInject;
using BL.Models;
using BL.Service;
using LightInject;
using System.Collections.Generic;
using System.Reflection;
using Web_mvc.LightInject;

namespace Web_mvc.App_Start
{
    public static class LightInjectConfig
    {
        public static void Congigurate()
        {
            var container = new ServiceContainer();
            container.RegisterControllers(Assembly.GetExecutingAssembly());

            container.EnablePerWebRequestScope();

            var config = new MapperConfiguration(cfg => cfg.AddProfiles(
                  new List<Profile>() { new WebAutomapperProfile(), new BLAutomapperProfile() }));

            container.Register(c => config.CreateMapper());

            container = BLLightInjectCongiguration.Congiguration(container);

            container.Register<IService<AuthorBL>, AuthorService>();
            container.Register<IService<GenreBL>, GenreService>();
            container.Register<IService<PeriodicalBL>, PeriodicalService>();
            //var resolver = new LightInjectWebApiDependencyResolver(container);             
            //DependencyResolver.SetResolver(new LightInjectMvcDependencyResolver(container));
            container.EnableMvc();
        }
    }
}