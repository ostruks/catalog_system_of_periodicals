﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web_mvc.Models
{
    public class GenreCreateModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Display(Name = "Жанр")]
        public string Title { get; set; }
        public ICollection<PeriodicalCreateModel> Periodicals { get; set; }
    }
}