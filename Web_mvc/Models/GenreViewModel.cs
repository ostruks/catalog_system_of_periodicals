﻿using System.ComponentModel.DataAnnotations;

namespace Web_mvc.Models
{
    public class GenreViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Display(Name = "Жанр")]
        public string Title { get; set; }
    }
}