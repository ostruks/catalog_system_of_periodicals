﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web_mvc.Models
{
    public class AuthorViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Display(Name = "Ф.И.О")]
        public string FullName { get; set; }
        [Display(Name = "Ник")]
        public string NickName { get; set; }
        [Display(Name = "Аватар")]
        public string Avatar { get; set; }
        [ScaffoldColumn(false)]
        public string UserId { get; set; }
        [Display(Name = "Издания")]
        public ICollection<PeriodicalViewModel> Periodicals { get; set; }
    }
}