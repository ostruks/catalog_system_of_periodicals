﻿using System.ComponentModel.DataAnnotations;

namespace Web_mvc.Models
{
    public class AuthorCreateModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [Display(Name = "Ник")]
        public string NickName { get; set; }
        [Display(Name = "Аватар")]
        public string Avatar { get; set; }
        [ScaffoldColumn(false)]
        public string UserId { get; set; }
    }
}