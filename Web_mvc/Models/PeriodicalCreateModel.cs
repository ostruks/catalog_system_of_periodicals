﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Web_mvc.Models
{
    public class PeriodicalCreateModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Display(Name = "Название")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 50 символов")]
        public string Title { get; set; }
        [Display(Name = "Периодичность выхода")]
        [Range(1, 92, ErrorMessage = "Недопустимая периодичность выхода")]
        public int? PublicationFrequencyDays { get; set; }
        [Display(Name = "Текст")]
        public string Text { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [ScaffoldColumn(false)]
        public int AuthorId { get; set; }
        [Display(Name = "Автор")]
        public AuthorCreateModel Author { get; set; }
        [ScaffoldColumn(false)]
        public ICollection<GenreCreateModel> Genres { get; set; }
        [Display(Name = "Жанры")]
        public List<int> GenresIds { get; set; }
        public IEnumerable<SelectListItem> GenreChoices { get; set; }
    }
}