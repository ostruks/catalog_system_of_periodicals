﻿using System.Collections.Generic;

namespace Web_mvc.Models
{
    public class PeriodicalIndexViewModel
    {
        public IEnumerable<PeriodicalViewModel> Periodicals { get; set; }
        public PageInfo PageInfo { get; set; }

        public PeriodicalIndexViewModel()
        {
            Periodicals = new List<PeriodicalViewModel>();
        }
    }
}