﻿using System.Collections.Generic;

namespace Web_mvc.Models
{
    public class PeriodicalAuthorCreateModel
    {
        public PeriodicalCreateModel PeriodicalCreateModel { get; set; }
        public IEnumerable<AuthorViewModel> Authors { get; set; }
        public PeriodicalAuthorCreateModel()
        {
            Authors = new List<AuthorViewModel>();
        }
    }
}