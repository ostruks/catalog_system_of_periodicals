﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web_mvc.Models
{
    public class PeriodicalViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Display(Name = "Название")]
        public string Title { get; set; }
        [Display(Name = "Периодичность выхода")]
        public int? PublicationFrequencyDays { get; set; }
        [Display(Name = "Текст")]
        public string Text { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Автор")]
        public AuthorViewModel Author { get; set; }
        [Display(Name = "Жанр")]
        public List<GenreViewModel> Genres { get; set; }
    }
}