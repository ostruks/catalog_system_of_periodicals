﻿using AutoMapper;
using BL.Models;
using BL.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_mvc.Models;

namespace Web_mvc.Controllers
{
    [Authorize]
    public class PeriodicalController : Controller
    {
        private readonly IService<AuthorBL> _authorService;
        private readonly IService<GenreBL> _genreService;
        private readonly IService<PeriodicalBL> _periodicalService;
        private readonly IMapper _mapper;
        public PeriodicalController(
            IService<PeriodicalBL> periodicalService,
            IService<AuthorBL> authorService,
            IService<GenreBL> genreService,
            IMapper mapper)
        {
            _authorService = authorService;
            _genreService = genreService;
            _periodicalService = periodicalService;
            _mapper = mapper;
        }

        // GET: Periodical
        [Authorize(Roles = "Author, Admin")]
        public ActionResult Index(int page = 1)
        {
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);
            int pageSize = 8;

            var listPeriodicalBL = _periodicalService.GetWithInclude("Author", "Genres");
            var periodicals = _mapper.Map<IEnumerable<PeriodicalViewModel>>(listPeriodicalBL);

            if (user != null)
            {
                var role = userManager.GetRoles(user.Id).FirstOrDefault();
                if (periodicals != null
                                && periodicals.Any(s => s.Author.UserId == user.Id))
                {
                    
                    IEnumerable<PeriodicalViewModel> periodicalsPerPages = periodicals.Where(s => s.Author.UserId == user.Id).Skip((page - 1) * pageSize).Take(pageSize);
                    PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = periodicals.Count() };
                    PeriodicalIndexViewModel pivm = new PeriodicalIndexViewModel { PageInfo = pageInfo, Periodicals = periodicalsPerPages };

                    return View(pivm);
                }
                else if(role == "Author")
                {
                    return RedirectToAction("Create", "Periodical");
                }
            }
            
            return RedirectToAction("Login", "Account");
        }

        public ActionResult GetAll(int page = 1)
        {
            var listPeriodicalBL = _periodicalService.GetWithInclude("Author", "Genres");
            var periodicals = _mapper.Map<IEnumerable<PeriodicalViewModel>>(listPeriodicalBL);
            int pageSize = 8;
            IEnumerable<PeriodicalViewModel> periodicalsPerPages = periodicals.Skip((page - 1) * pageSize).Take(pageSize);
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = periodicals.Count() };
            PeriodicalIndexViewModel pivm = new PeriodicalIndexViewModel { PageInfo = pageInfo, Periodicals = periodicalsPerPages };

            return View(pivm);
        }

        // GET: Periodical/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ReturnUrl = Request.UrlReferrer;

            var PeriodicalBL = _periodicalService.FindByIdWithIncludes(id.Value, new string[] { "Author" });

            if (PeriodicalBL == null)
            {
                return HttpNotFound();
            }
            var periodical = _mapper.Map<PeriodicalViewModel>(PeriodicalBL);

            return View(periodical);
        }

        // GET: Periodical/Create
        [Authorize(Roles = "Author, Admin")]
        public ActionResult Create()
        {
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);

            var author = _authorService.GetAll().Where(s => s.UserId == user.Id).FirstOrDefault();
            var listGenreBL = _genreService.GetAll();
            var genres = _mapper.Map<IEnumerable<GenreViewModel>>(listGenreBL);
            PeriodicalCreateModel periodicalCreateModel = new PeriodicalCreateModel();
            periodicalCreateModel.GenreChoices = genres.Select(m => new SelectListItem
            {
                Text = m.Title,
                Value = m.Id.ToString()
            });

            periodicalCreateModel.GenresIds = genres.Select(m => m.Id).ToList();

            ViewBag.ReturnUrl = Request.UrlReferrer;

            if (author == null)
            {
                PeriodicalAuthorCreateModel periodicalAuthorCreateModel = new PeriodicalAuthorCreateModel()
                {
                    PeriodicalCreateModel = periodicalCreateModel,
                    Authors = _mapper.Map<IEnumerable<AuthorViewModel>>(_authorService.GetAll())
                };

                return View("CreateByAdmin", periodicalAuthorCreateModel);
            }
            else
            {
                periodicalCreateModel.AuthorId = _mapper.Map<AuthorCreateModel>(author).Id;
            }

            return View(periodicalCreateModel);
        }

        // POST: Periodical/Create
        [HttpPost]
        public ActionResult Create(PeriodicalCreateModel periodicalCreateModel)
        {
            try
            {
                var listGenreBL = _genreService.GetWithInclude("Periodicals");
                var genres = _mapper.Map<IEnumerable<GenreCreateModel>>(listGenreBL).Where(s => periodicalCreateModel.GenresIds.Contains(s.Id)).ToList();

                if (ModelState.IsValid)
                {
                    periodicalCreateModel.Genres = genres;

                    var periodical = _mapper.Map<PeriodicalBL>(periodicalCreateModel);
                    _periodicalService.Create(periodical);
                }

                return RedirectToAction("Index", "Periodical");
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "Author, Admin")]
        public ActionResult CreateByAdmin()
        {
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);

            var author = _authorService.GetAll().Where(s => s.UserId == user.Id).FirstOrDefault();
            var listGenreBL = _genreService.GetAll();
            var genres = _mapper.Map<IEnumerable<GenreViewModel>>(listGenreBL);
            PeriodicalCreateModel periodicalCreateModel = new PeriodicalCreateModel();
            periodicalCreateModel.GenreChoices = genres.Select(m => new SelectListItem
            {
                Text = m.Title,
                Value = m.Id.ToString()
            });

            periodicalCreateModel.GenresIds = genres.Select(m => m.Id).ToList();

            ViewBag.ReturnUrl = Request.UrlReferrer;

            PeriodicalAuthorCreateModel periodicalAuthorCreateModel = new PeriodicalAuthorCreateModel()
            {
                PeriodicalCreateModel = periodicalCreateModel,
                Authors = _mapper.Map<IEnumerable<AuthorViewModel>>(_authorService.GetAll())
            };

            return View("CreateByAdmin", periodicalAuthorCreateModel);
        }

        [HttpPost]
        public ActionResult CreateByAdmin(PeriodicalAuthorCreateModel periodicalAuthorCreateModel)
        {
            try
            {
                PeriodicalCreateModel periodicalCreateModel = periodicalAuthorCreateModel.PeriodicalCreateModel;
                var listGenreBL = _genreService.GetWithInclude("Periodicals");
                var genres = _mapper.Map<IEnumerable<GenreCreateModel>>(listGenreBL).Where(s => periodicalCreateModel.GenresIds.Contains(s.Id)).ToList();

                if (ModelState.IsValid)
                {
                    periodicalCreateModel.Genres = genres;

                    var periodical = _mapper.Map<PeriodicalBL>(periodicalCreateModel);
                    _periodicalService.Create(periodical);
                }

                return RedirectToAction("Index", "Periodical");
            }
            catch
            {
                return View();
            }
        }

        // GET: Periodical/Edit/5
        [Authorize(Roles = "Author, Admin")]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ReturnUrl = Request.UrlReferrer;

            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);

            var author = _authorService.GetAll().Where(s => s.UserId == user.Id).FirstOrDefault();
            var role = userManager.GetRoles(user.Id).FirstOrDefault();
            var PeriodicalBL = _periodicalService.FindByIdWithIncludes(id.Value, new string[] { "Author" });

            if (PeriodicalBL == null)
            {
                return HttpNotFound();
            }

            var periodical = _mapper.Map<PeriodicalCreateModel>(PeriodicalBL);

            if((author == null && role == "Admin") || (author.Id == periodical.Author.Id))
            {
                return View(periodical);
            }

            return RedirectToAction("GetAll", "Periodical");
        }

        // POST: Periodical/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PeriodicalCreateModel periodicalUpadteModel)
        {
            try
            {
                periodicalUpadteModel.AuthorId = periodicalUpadteModel.Author.Id;
                var periodical = _mapper.Map<PeriodicalBL>(periodicalUpadteModel);

                _periodicalService.Update(periodical);

                return RedirectToAction("GetAll", "Periodical");
            }
            catch
            {
                return View();
            }
        }

        // GET: Periodical/Delete/5
        [Authorize(Roles = "Author, Admin")]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            ViewBag.ReturnUrl = Request.UrlReferrer;

            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);

            var author = _authorService.GetAll().Where(s => s.UserId == user.Id).FirstOrDefault();
            var role = userManager.GetRoles(user.Id).FirstOrDefault();
            var PeriodicalBL = _periodicalService.GetWithInclude("Author").Where(s => s.Id == id.Value).FirstOrDefault();

            if (PeriodicalBL == null)
            {
                return HttpNotFound();
            }

            var periodical = _mapper.Map<PeriodicalViewModel>(PeriodicalBL);

            if ((author == null && role == "Admin") || author.Id == periodical.Author.Id)
            {
                return View(periodical);
            }
            else
            {
                return RedirectToAction("GetAll", "Periodical");
            }
        }

        // POST: Periodical/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, PeriodicalViewModel periodical)
        {
            try
            {
                _periodicalService.DeleteById(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
