﻿using AutoMapper;
using BL.Models;
using BL.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_mvc.Models;

namespace Web_mvc.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IService<AuthorBL> _authorService;
        private readonly IMapper _mapper;
        public AuthorController(IService<AuthorBL> authorService, IMapper mapper)
        {
            _authorService = authorService;
            _mapper = mapper;
        }

        // GET: Author
        [Authorize]
        public ActionResult Index()
        {
            var listAuthorBL = _authorService.GetWithInclude("Periodicals");
            var authors = _mapper.Map<IEnumerable<AuthorViewModel>>(listAuthorBL);

            if (authors != null)
            {
                return View(authors);
            }
            return RedirectToAction("Login", "Account");
        }

        // GET: Author/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            var BLAuthor = _authorService.FindByIdWithIncludes(id.Value, new string[] { "Periodicals" });
            if (BLAuthor == null)
            {
                return HttpNotFound();
            }
            var author = _mapper.Map<AuthorViewModel>(BLAuthor);

            ViewBag.ReturnUrl = Request.UrlReferrer;

            return View(author);
        }

        // GET: Author/Create
        [Authorize(Roles = "Admin, User")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Author/Create
        [HttpPost]
        public ActionResult Create(AuthorCreateModel authorCreateModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
                    ApplicationUser user = userManager.FindByEmail(User.Identity.Name);

                    var oldRoleId = user.Roles.SingleOrDefault().RoleId;
                    var oldRoleName = userManager.GetRoles(user.Id).FirstOrDefault();

                    if(oldRoleName != "Admin")
                    {
                        if (oldRoleName != "Author")
                        {
                            userManager.RemoveFromRole(user.Id, oldRoleName);
                            userManager.AddToRole(user.Id, "Author");
                            authorCreateModel.UserId = user.Id;
                        }
                        else
                        {
                            return RedirectToAction("Index", "Author");
                        }
                    }
                }

                var author = _mapper.Map<AuthorBL>(authorCreateModel);
                _authorService.Create(author);

                return RedirectToAction("Index", "Author");
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "Admin, Author")]
        public ActionResult EditFromManage()
        {
            ApplicationUserManager userManager = HttpContext.GetOwinContext()
                                            .GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindByEmail(User.Identity.Name);

            var BLAuthor = _authorService.GetWithInclude("Periodicals").Where(s => s.UserId == user.Id).FirstOrDefault();
            var author = _mapper.Map<AuthorCreateModel>(BLAuthor);

            return View(author);
        }

        [HttpPost]
        public ActionResult EditFromManage(AuthorCreateModel authorUpdateModel)
        {
            try
            {
                var author = _mapper.Map<AuthorBL>(authorUpdateModel);
                _authorService.Update(author);

                return RedirectToAction("Index", "Author");
            }
            catch
            {
                return View();
            }
        }

        // GET: Author/Edit/5
        [Authorize(Roles = "Admin, Author")]
        public ActionResult Edit(int id)
        {
            var BLAuthor = _authorService.FindById(id);
            var author = _mapper.Map<AuthorCreateModel>(BLAuthor);

            return View(author);
        }

        // POST: Author/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AuthorCreateModel authorUpdateModel)
        {
            try
            {
                var author = _mapper.Map<AuthorBL>(authorUpdateModel);
                _authorService.Update(author);

                return RedirectToAction("Index", "Author");
            }
            catch
            {
                return View();
            }
        }

        // GET: Author/Delete/5
        [Authorize(Roles = "Admin, Author")]
        public ActionResult Delete(int id)
        {
            var BLAuthor = _authorService.FindById(id);
            var author = _mapper.Map<AuthorViewModel>(BLAuthor);

            return View(author);
        }

        // POST: Author/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, AuthorViewModel author)
        {
            try
            {
                _authorService.DeleteById(id);

                return RedirectToAction("Index", "Author");
            }
            catch
            {
                return View();
            }
        }
    }
}
