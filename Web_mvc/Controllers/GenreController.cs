﻿using AutoMapper;
using BL.Models;
using BL.Service;
using System.Collections.Generic;
using System.Web.Mvc;
using Web_mvc.Models;

namespace Web_mvc.Controllers
{
    public class GenreController : Controller
    {
        private readonly IService<GenreBL> _genreService;
        private readonly IMapper _mapper;
        public GenreController(IService<GenreBL> genreService, IMapper mapper)
        {
            _genreService = genreService;
            _mapper = mapper;
        }

        // GET: Genre
        [Authorize]
        public ActionResult Index()
        {
            var listGenreBL = _genreService.GetAll();
            var genres = _mapper.Map<IEnumerable<GenreViewModel>>(listGenreBL);

            if (genres != null)
            {
                return View(genres);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Genre/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }
            var GenreBL = _genreService.FindById(id.Value);
            var genre = _mapper.Map<GenreViewModel>(GenreBL);

            ViewBag.ReturnUrl = Request.UrlReferrer;

            return View(genre);
        }

        // GET: Genre/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Genre/Create
        [HttpPost]
        public ActionResult Create(GenreCreateModel genreCreateModel)
        {
            try
            {
                var genre = _mapper.Map<GenreBL>(genreCreateModel);
                _genreService.Create(genre);

                return RedirectToAction("Index", "Genre");
            }
            catch
            {
                return View();
            }
        }

        // GET: Genre/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var GenreBL = _genreService.FindById(id);
            var genre = _mapper.Map<GenreCreateModel>(GenreBL);

            return View(genre);
        }

        // POST: Genre/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, GenreCreateModel genreUpdateModel)
        {
            try
            {
                var genre = _mapper.Map<GenreBL>(genreUpdateModel);
                _genreService.Update(genre);

                return RedirectToAction("Index", "Genre");
            }
            catch
            {
                return View();
            }
        }

        // GET: Genre/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            var GenreBL = _genreService.FindById(id);
            var genre = _mapper.Map<GenreViewModel>(GenreBL);

            return View(genre);
        }

        // POST: Genre/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, GenreViewModel genre)
        {
            try
            {
                _genreService.DeleteById(id);

                return RedirectToAction("Index", "Genre");
            }
            catch
            {
                return View();
            }
        }
    }
}
