using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web_mvc.App_Start;

namespace Web_mvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            LightInjectConfig.Congigurate();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
